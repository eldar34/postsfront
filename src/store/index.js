import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    backendUrl: "https://load2backend.herokuapp.com/api/v1",
    posts: {},
  },
  mutations: {
    setPosts(state, payload){
      state.posts = payload
    }
  },
  actions: {
    async getServerPosts ({ commit, getters }) {
      let getPosts = await fetch(
        `${getters.getServerUrl}/posts/`
      ).then(response => response.json());
      commit('setPosts', getPosts);
      
    },

    async deletePost({ commit, getters, dispatch}, postid){
        await fetch(
          `${getters.getServerUrl}/posts/${postid}/`,
          {
            method: "delete",
            headers: {
              "Content-Type": "application/json"
            }
          }
        );
        await dispatch('getServerPosts');
        
    }
  },
  modules: {
  },
  getters: {
    getServerUrl: state => {
        return state.backendUrl
    },
    getPosts: (state, ) => {
        return state.posts
    }
  } 
})
