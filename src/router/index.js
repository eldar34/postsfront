import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import NewPost from '../views/NewPost.vue'
import Posts from '../views/Posts.vue'
import Single from '../views/Single.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/addpost',
    name: 'NewPost',
    component: NewPost
  },
  {
    path: '/posts',
    name: 'Posts',
    component: Posts
  },
  {
    path: '/:id',
    name: 'Single',
    component: Single,
    props: true
  },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
